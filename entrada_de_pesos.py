# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'entrada_de_pesos.ui'
#
# Created: Fri Sep 05 16:25:37 2014
#      by: PyQt4 UI code generator 4.11.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(805, 567)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(710, 530, 81, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Close)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.comboBox = QtGui.QComboBox(Dialog)
        self.comboBox.setGeometry(QtCore.QRect(10, 10, 781, 21))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.comboBox.addItem(_fromUtf8(""))
        self.groupBox = QtGui.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 40, 781, 481))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.plainTextEdit = QtGui.QPlainTextEdit(self.groupBox)
        self.plainTextEdit.setGeometry(QtCore.QRect(10, 50, 761, 421))
        self.plainTextEdit.setObjectName(_fromUtf8("plainTextEdit"))
        self.toolButton = QtGui.QToolButton(self.groupBox)
        self.toolButton.setGeometry(QtCore.QRect(610, 20, 161, 20))
        self.toolButton.setObjectName(_fromUtf8("toolButton"))
        self.toolButton_2 = QtGui.QToolButton(self.groupBox)
        self.toolButton_2.setGeometry(QtCore.QRect(440, 20, 161, 20))
        self.toolButton_2.setObjectName(_fromUtf8("toolButton_2"))
        self.toolButton_3 = QtGui.QToolButton(self.groupBox)
        self.toolButton_3.setGeometry(QtCore.QRect(270, 20, 161, 20))
        self.toolButton_3.setObjectName(_fromUtf8("toolButton_3"))

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.comboBox.setItemText(0, _translate("Dialog", "\\\\Exibir seguinte texto antes que o usuário clique: \"Superfície de Rugosidade\"", None))
        self.groupBox.setTitle(_translate("Dialog", "Planilha de Pesos", None))
        self.plainTextEdit.setPlainText(_translate("Dialog", "//Exibir aqui a planilha de de pesos para o objetivo de estudo selecionado (Superfície de Rugosidade) - modelo de planilha no formato excel (\"adiçao de pesos - tabela modelo\")", None))
        self.toolButton.setText(_translate("Dialog", "Ligar Edição", None))
        self.toolButton_2.setText(_translate("Dialog", "Salvar Edição", None))
        self.toolButton_3.setText(_translate("Dialog", "Cancelar Edição", None))

