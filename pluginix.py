# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginIX
                                 A QGIS plugin
 Plugin
                              -------------------
        begin                : 2014-09-05
        copyright            : (C) 2014 by LampD
        email                : contato@lampd.com.br
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import os
import urllib
from string import Template
from string import capwords
import datetime
import codecs
# Import the PyQt and QGIS libraries

from qgis.core import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from pluginixdialog import PluginIXDialog
import os.path

import sys
from PyQt4 import *

from PyQt4.QtCore import *
from PyQt4.QtGui import *
#from Tkinter import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class PluginIX:

    global linha
    global coluna

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'pluginix_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/pluginix/icon.png"),
            u"Plugin", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&PluginIx", self.action)

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&PluginIx", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        self.dlg = PluginIXDialog()

        global temTab
        global dictTabs
        self.temTab = False
        self.dictTabs = {}

        PluginIX.linha = 0
        PluginIX.coluna = 0

        QObject.connect(self.dlg.botaoDefProjDatum, SIGNAL("clicked()"), self.abreArquivos)
        QObject.connect(self.dlg.botaoAreaEstudo, SIGNAL("clicked()"), self.abreArquivos)
        QObject.connect(self.dlg.botaoPontoInicial, SIGNAL("clicked()"), self.abreArquivos)
        QObject.connect(self.dlg.botaoPontoFinal, SIGNAL("clicked()"), self.abreArquivos)

        QObject.connect(self.dlg.checkBoxCustoMonet, SIGNAL("clicked()"), self.addDefinicaoCriterios)
        QObject.connect(self.dlg.checkBoxAHPEcono, SIGNAL("clicked()"), self.addDefinicaoCriterios)
        QObject.connect(self.dlg.checkBoxAHPRisco, SIGNAL("clicked()"), self.addDefinicaoCriterios)
        QObject.connect(self.dlg.checkBoxAHPAmbient, SIGNAL("clicked()"), self.addDefinicaoCriterios)

        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # do something useful (delete the line containing pass and
            # substitute with your code)
            pass

    def abreArquivos(self):
       
        fname = QtGui.QFileDialog.getOpenFileName(self.dlg, 'Open file', '/')
        
        if(self.dlg.sender().objectName() == "botaoDefProjDatum"):
            self.dlg.textoDefProjDatum.setText(fname)

        if(self.dlg.sender().objectName() == "botaoAreaEstudo"):
            self.dlg.textoAreaEstudo.setText(fname)

        if(self.dlg.sender().objectName() == "botaoPontoInicial"):
            self.dlg.textoPontoInicial.setText(fname)

        if(self.dlg.sender().objectName() == "botaoPontoFinal"):
            self.dlg.textoPontoFinal.setText(fname)


    def addDefinicaoCriterios(self):
        name = self.dlg.sender().text().split()
        nome = ""
        for i in range (0, len(name)):
            nome += name[i][:1]
        nome = nome.upper()

        self.dictTabs[self.dlg.sender().objectName()] = nome

        self.tab_3 = QtGui.QWidget()
        self.tab_3.setObjectName(nome)
        self.tab_3.setGeometry(QtCore.QRect(0, 0, 465, 230))

        if(self.temTab == False):
            self.tabWidget_2 = QtGui.QTabWidget()
            self.tabWidget_2.setGeometry(QtCore.QRect(10, 20, 465, 230))
            self.tabWidget_2.setObjectName(_fromUtf8("tabWidget_2"))

            self.temTab = True

        self.scrollArea_2 = QtGui.QScrollArea(self.tab_3)
        self.scrollArea_2.setGeometry(QRect(10, 10, 465, 230))
        self.scrollArea_2.setMouseTracking(False)
        self.scrollArea_2.setToolTip("")
        self.scrollArea_2.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollArea_2.setWidgetResizable(False)
        self.scrollArea_2.setObjectName("scrollArea_2")

        self.scrollAreaWidgetContents_2 = QtGui.QWidget()
        self.scrollAreaWidgetContents_2.setGeometry(QtCore.QRect(0, 0, 465, 230))
        self.scrollAreaWidgetContents_2.setObjectName(_fromUtf8("scrollAreaWidgetContents_2"))

        if(self.dlg.sender().isChecked()):
            self.checkBox = QtGui.QCheckBox(self.scrollAreaWidgetContents_2)
            self.checkBox.setGeometry(QRect(10, 10, 101, 17))
            self.checkBox.setObjectName("checkBox")
            self.checkBox_2 = QtGui.QCheckBox(self.scrollAreaWidgetContents_2)
            self.checkBox_2.setGeometry(QRect(10, 30, 101, 17))
            self.checkBox_2.setObjectName("checkBox_2")
            self.checkBox_5 = QtGui.QCheckBox(self.scrollAreaWidgetContents_2)
            self.checkBox_5.setGeometry(QRect(10, 90, 171, 17))
            self.checkBox_5.setObjectName("checkBox_5")
            self.checkBox_3 = QtGui.QCheckBox(self.scrollAreaWidgetContents_2)
            self.checkBox_3.setGeometry(QRect(10, 50, 181, 17))
            self.checkBox_3.setObjectName("checkBox_3")
            self.checkBox_4 = QtGui.QCheckBox(self.scrollAreaWidgetContents_2)
            self.checkBox_4.setGeometry(QRect(10, 70, 151, 17))
            self.checkBox_4.setObjectName("checkBox_4")
            self.checkBox_6 = QtGui.QCheckBox(self.scrollAreaWidgetContents_2)
            self.checkBox_6.setGeometry(QRect(10, 110, 161, 17))
            self.checkBox_6.setObjectName("checkBox_6")
            self.checkBox_7 = QtGui.QCheckBox(self.scrollAreaWidgetContents_2)
            self.checkBox_7.setGeometry(QRect(10, 130, 191, 16))
            self.checkBox_7.setObjectName("checkBox_7")
            self.checkBox_9 = QtGui.QCheckBox(self.scrollAreaWidgetContents_2)
            self.checkBox_9.setGeometry(QRect(10, 150, 121, 17))
            self.checkBox_9.setObjectName("checkBox_9")
            self.checkBox_17 = QtGui.QCheckBox(self.scrollAreaWidgetContents_2)
            self.checkBox_17.setGeometry(QRect(10, 170, 121, 17))
            self.checkBox_17.setObjectName("checkBox_17")
            self.scrollArea_2.setWidget(self.scrollAreaWidgetContents_2)

            self.dlg.groupBox_2.setTitle(_translate("PluginIX", "3 - Definição de critérios", None))
            self.checkBox.setText(_translate("PluginIX", "Aeroportos", None))
            self.checkBox_2.setText(_translate("PluginIX", "Áreas Alagadas", None))
            self.checkBox_5.setText(_translate("PluginIX", "Sistema Interligado Nacional", None))
            self.checkBox_3.setText(_translate("PluginIX", "Hidrografia por Faixas de Largura", None))
            self.checkBox_4.setText(_translate("PluginIX", "Rios Navegáveis", None))
            self.checkBox_6.setText(_translate("PluginIX", "Unidades de Conservação", None))
            self.checkBox_7.setText(_translate("PluginIX", "APPs", None))
            self.checkBox_9.setText(_translate("PluginIX", "Uso do Solo", None))
            self.checkBox_17.setText(_translate("PluginIX", "Estruturas", None))
            self.tabWidget_2.setTabText(self.tabWidget_2.indexOf(self.tab_3), nome)

            QObject.connect(self.checkBox, SIGNAL("clicked()"), self.addComboEntrArquivos)
            QObject.connect(self.checkBox_2, SIGNAL("clicked()"), self.addComboEntrArquivos)
            QObject.connect(self.checkBox_5, SIGNAL("clicked()"), self.addComboEntrArquivos)
            QObject.connect(self.checkBox_3, SIGNAL("clicked()"), self.addComboEntrArquivos)
            QObject.connect(self.checkBox_4, SIGNAL("clicked()"), self.addComboEntrArquivos)
            QObject.connect(self.checkBox_6, SIGNAL("clicked()"), self.addComboEntrArquivos)
            QObject.connect(self.checkBox_7, SIGNAL("clicked()"), self.addComboEntrArquivos)
            QObject.connect(self.checkBox_9, SIGNAL("clicked()"), self.addComboEntrArquivos)
            QObject.connect(self.checkBox_17, SIGNAL("clicked()"), self.addComboEntrArquivos)

            self.tabWidget_2.addTab(self.tab_3, nome)

            self.dlg.horizontalLayout.addWidget(self.tabWidget_2)

            self.dlg.botaoAltEdiCriterios.setEnabled(True)

        else:
            nome = self.dictTabs[self.dlg.sender().objectName()]
            print(nome)

            tab = self.tabWidget_2.findChildren(QtGui.QWidget, nome)
            tab[0].setParent(None)

    def addComboEntrArquivos(self):
        self.comboArea = QtGui.QWidget()
        self.comboArea.setGeometry(QtCore.QRect(250, 25, 0, 0))
        self.comboArea.setObjectName(_fromUtf8("area"+self.dlg.sender().objectName()))
        self.comboArea.setContentsMargins(20, 20, 20, 20)

        self.lineEdit = QtGui.QLineEdit(self.comboArea)
        self.lineEdit.setGeometry(QtCore.QRect(0, 0, 150, 20))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))

        self.botao = QtGui.QToolButton(self.comboArea)
        self.botao.setGeometry(QtCore.QRect(110, 0, 60, 20))
        self.botao.setObjectName(_fromUtf8("botao"))
        self.botao.setText(_translate("PluginIX", "...", None))

        if(PluginIX.linha == 4):
            PluginIX.coluna += 1
            PluginIX.linha = 0

        self.dlg.gridLayout.addWidget(self.comboArea, PluginIX.linha, PluginIX.coluna, 1, 1)
        PluginIX.linha += 1

