# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_pluginix.ui'
#
# Created: Tue Sep 16 10:59:14 2014
#      by: PyQt4 UI code generator 4.11.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_PluginIX(object):
    def setupUi(self, PluginIX):
        PluginIX.setObjectName(_fromUtf8("PluginIX"))
        PluginIX.resize(1018, 679)
        self.tabWidget = QtGui.QTabWidget(PluginIX)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 1011, 641))
        self.tabWidget.setDocumentMode(False)
        self.tabWidget.setTabsClosable(False)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.info = QtGui.QTextEdit(self.tab)
        self.info.setGeometry(QtCore.QRect(750, 10, 251, 221))
        self.info.setObjectName(_fromUtf8("info"))
        self.groupSupRugosidade = QtGui.QGroupBox(self.tab)
        self.groupSupRugosidade.setGeometry(QtCore.QRect(10, 150, 201, 311))
        self.groupSupRugosidade.setFlat(False)
        self.groupSupRugosidade.setCheckable(False)
        self.groupSupRugosidade.setObjectName(_fromUtf8("groupSupRugosidade"))
        self.scrollSupRugosidade = QtGui.QScrollArea(self.groupSupRugosidade)
        self.scrollSupRugosidade.setGeometry(QtCore.QRect(10, 50, 181, 251))
        self.scrollSupRugosidade.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollSupRugosidade.setWidgetResizable(False)
        self.scrollSupRugosidade.setObjectName(_fromUtf8("scrollSupRugosidade"))
        self.scrollAreaWidgetContents = QtGui.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 152, 159))
        self.scrollAreaWidgetContents.setObjectName(_fromUtf8("scrollAreaWidgetContents"))
        self.checkBoxCustoMonet = QtGui.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBoxCustoMonet.setGeometry(QtCore.QRect(10, 10, 121, 17))
        self.checkBoxCustoMonet.setObjectName(_fromUtf8("checkBoxCustoMonet"))
        self.checkBoxAHPEcono = QtGui.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBoxAHPEcono.setGeometry(QtCore.QRect(10, 30, 121, 17))
        self.checkBoxAHPEcono.setObjectName(_fromUtf8("checkBoxAHPEcono"))
        self.checkBoxAHPRisco = QtGui.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBoxAHPRisco.setGeometry(QtCore.QRect(10, 50, 70, 17))
        self.checkBoxAHPRisco.setObjectName(_fromUtf8("checkBoxAHPRisco"))
        self.checkBoxAHPAmbient = QtGui.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBoxAHPAmbient.setGeometry(QtCore.QRect(10, 70, 111, 17))
        self.checkBoxAHPAmbient.setObjectName(_fromUtf8("checkBoxAHPAmbient"))
        self.scrollSupRugosidade.setWidget(self.scrollAreaWidgetContents)
        self.lineEdit = QtGui.QLineEdit(self.groupSupRugosidade)
        self.lineEdit.setGeometry(QtCore.QRect(10, 20, 111, 20))
        self.lineEdit.setReadOnly(False)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.botaoAdSupRugosidade = QtGui.QToolButton(self.groupSupRugosidade)
        self.botaoAdSupRugosidade.setGeometry(QtCore.QRect(130, 20, 61, 19))
        self.botaoAdSupRugosidade.setObjectName(_fromUtf8("botaoAdSupRugosidade"))
        self.groupAreaEstudo = QtGui.QGroupBox(self.tab)
        self.groupAreaEstudo.setGeometry(QtCore.QRect(10, 0, 521, 141))
        self.groupAreaEstudo.setObjectName(_fromUtf8("groupAreaEstudo"))
        self.botaoAreaEstudo = QtGui.QToolButton(self.groupAreaEstudo)
        self.botaoAreaEstudo.setGeometry(QtCore.QRect(430, 50, 81, 21))
        self.botaoAreaEstudo.setObjectName(_fromUtf8("botaoAreaEstudo"))
        self.botaoPontoInicial = QtGui.QToolButton(self.groupAreaEstudo)
        self.botaoPontoInicial.setGeometry(QtCore.QRect(430, 80, 81, 21))
        self.botaoPontoInicial.setObjectName(_fromUtf8("botaoPontoInicial"))
        self.botaoPontoFinal = QtGui.QToolButton(self.groupAreaEstudo)
        self.botaoPontoFinal.setGeometry(QtCore.QRect(430, 110, 81, 21))
        self.botaoPontoFinal.setObjectName(_fromUtf8("botaoPontoFinal"))
        self.botaoDefProjDatum = QtGui.QToolButton(self.groupAreaEstudo)
        self.botaoDefProjDatum.setGeometry(QtCore.QRect(430, 20, 81, 21))
        self.botaoDefProjDatum.setObjectName(_fromUtf8("botaoDefProjDatum"))
        self.textoDefProjDatum = QtGui.QLineEdit(self.groupAreaEstudo)
        self.textoDefProjDatum.setGeometry(QtCore.QRect(10, 20, 411, 20))
        self.textoDefProjDatum.setInputMask(_fromUtf8(""))
        self.textoDefProjDatum.setObjectName(_fromUtf8("textoDefProjDatum"))
        self.textoAreaEstudo = QtGui.QLineEdit(self.groupAreaEstudo)
        self.textoAreaEstudo.setGeometry(QtCore.QRect(10, 50, 411, 20))
        self.textoAreaEstudo.setObjectName(_fromUtf8("textoAreaEstudo"))
        self.textoPontoInicial = QtGui.QLineEdit(self.groupAreaEstudo)
        self.textoPontoInicial.setGeometry(QtCore.QRect(10, 80, 411, 20))
        self.textoPontoInicial.setObjectName(_fromUtf8("textoPontoInicial"))
        self.textoPontoFinal = QtGui.QLineEdit(self.groupAreaEstudo)
        self.textoPontoFinal.setGeometry(QtCore.QRect(10, 110, 411, 20))
        self.textoPontoFinal.setObjectName(_fromUtf8("textoPontoFinal"))
        self.groupPesos = QtGui.QGroupBox(self.tab)
        self.groupPesos.setGeometry(QtCore.QRect(730, 400, 271, 71))
        self.groupPesos.setObjectName(_fromUtf8("groupPesos"))
        self.botaoPesos = QtGui.QToolButton(self.groupPesos)
        self.botaoPesos.setGeometry(QtCore.QRect(10, 20, 121, 41))
        self.botaoPesos.setObjectName(_fromUtf8("botaoPesos"))
        self.groupBox_5 = QtGui.QGroupBox(self.tab)
        self.groupBox_5.setGeometry(QtCore.QRect(10, 470, 991, 141))
        self.groupBox_5.setObjectName(_fromUtf8("groupBox_5"))
        self.gridLayoutWidget = QtGui.QWidget(self.groupBox_5)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(9, 19, 971, 111))
        self.gridLayoutWidget.setObjectName(_fromUtf8("gridLayoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.groupBox_2 = QtGui.QGroupBox(self.tab)
        self.groupBox_2.setGeometry(QtCore.QRect(220, 150, 501, 311))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.botaoAltEdiCriterios = QtGui.QToolButton(self.groupBox_2)
        self.botaoAltEdiCriterios.setEnabled(False)
        self.botaoAltEdiCriterios.setGeometry(QtCore.QRect(370, 10, 121, 20))
        self.botaoAltEdiCriterios.setObjectName(_fromUtf8("botaoAltEdiCriterios"))
        self.horizontalLayoutWidget = QtGui.QWidget(self.groupBox_2)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(9, 39, 491, 261))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.buttonBox = QtGui.QDialogButtonBox(PluginIX)
        self.buttonBox.setGeometry(QtCore.QRect(850, 650, 161, 20))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Close|QtGui.QDialogButtonBox.Help)
        self.buttonBox.setCenterButtons(False)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))

        self.retranslateUi(PluginIX)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), PluginIX.reject)
        QtCore.QMetaObject.connectSlotsByName(PluginIX)

    def retranslateUi(self, PluginIX):
        PluginIX.setWindowTitle(_translate("PluginIX", "Dialog", None))
        self.info.setHtml(_translate("PluginIX", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">//CAIXA DE INFORMAÇÕES E ORIENTAÇÕES</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; font-weight:600;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">//Para cada um dos Item do plugin esta caixa deverá exibir orientações e descrição de metodologia. Quando cada item for acessado as informações deverão ser exibidas. </span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; font-weight:600;\"><br /></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; font-weight:600;\"><br /></p></body></html>", None))
        self.groupSupRugosidade.setTitle(_translate("PluginIX", "2 - Superfícies de rugosidade", None))
        self.checkBoxCustoMonet.setText(_translate("PluginIX", "Custos monetários", None))
        self.checkBoxAHPEcono.setText(_translate("PluginIX", "AHP Econômico", None))
        self.checkBoxAHPRisco.setText(_translate("PluginIX", "AHP Risco", None))
        self.checkBoxAHPAmbient.setText(_translate("PluginIX", "AHP Ambiental", None))
        self.lineEdit.setPlaceholderText(_translate("PluginIX", "Outros", None))
        self.botaoAdSupRugosidade.setText(_translate("PluginIX", "Adicionar", None))
        self.groupAreaEstudo.setTitle(_translate("PluginIX", "1 - Área de estudo", None))
        self.botaoAreaEstudo.setText(_translate("PluginIX", "...", None))
        self.botaoPontoInicial.setText(_translate("PluginIX", "...", None))
        self.botaoPontoFinal.setText(_translate("PluginIX", "...", None))
        self.botaoDefProjDatum.setText(_translate("PluginIX", "...", None))
        self.textoDefProjDatum.setPlaceholderText(_translate("PluginIX", "Definicao de Projeção e Datum", None))
        self.textoAreaEstudo.setPlaceholderText(_translate("PluginIX", "Adicionar arquivo com área de estudo (shapefile)", None))
        self.textoPontoInicial.setPlaceholderText(_translate("PluginIX", "Adicionar ponto inicial da rota (shapefile)", None))
        self.textoPontoFinal.setPlaceholderText(_translate("PluginIX", "Adicionar arquivo com ponto final da rota (shapefile)", None))
        self.groupPesos.setTitle(_translate("PluginIX", "4 - Inserção de pesos para superficies de rugosidade", None))
        self.botaoPesos.setText(_translate("PluginIX", "Entrada de pesos", None))
        self.groupBox_5.setTitle(_translate("PluginIX", "5 - Entrada de arquivos vetoriais (critérios)", None))
        self.groupBox_2.setTitle(_translate("PluginIX", "3 - Definição de critérios", None))
        self.botaoAltEdiCriterios.setText(_translate("PluginIX", "Edição de critérios", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("PluginIX", "Parâmetros de projeto", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("PluginIX", "Processamento de dados", None))
        self.buttonBox.setToolTip(_translate("PluginIX", "dsds", None))

