# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginIX
                                 A QGIS plugin
 Plugin
                             -------------------
        begin                : 2014-09-05
        copyright            : (C) 2014 by LampD
        email                : contato@lampd.com.br
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface):
    # load PluginIX class from file PluginIX
    from pluginix import PluginIX
    return PluginIX(iface)
