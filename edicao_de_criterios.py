# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'edicao_de_criterios.ui'
#
# Created: Fri Sep 05 16:25:20 2014
#      by: PyQt4 UI code generator 4.11.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(846, 472)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(680, 440, 161, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.groupBox = QtGui.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(430, 10, 411, 431))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.comboBox = QtGui.QComboBox(self.groupBox)
        self.comboBox.setGeometry(QtCore.QRect(10, 20, 281, 22))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.toolButton_3 = QtGui.QToolButton(self.groupBox)
        self.toolButton_3.setGeometry(QtCore.QRect(300, 20, 101, 21))
        self.toolButton_3.setObjectName(_fromUtf8("toolButton_3"))
        self.toolButton = QtGui.QToolButton(self.groupBox)
        self.toolButton.setGeometry(QtCore.QRect(300, 400, 101, 21))
        self.toolButton.setObjectName(_fromUtf8("toolButton"))
        self.toolButton_4 = QtGui.QToolButton(self.groupBox)
        self.toolButton_4.setGeometry(QtCore.QRect(190, 400, 101, 21))
        self.toolButton_4.setObjectName(_fromUtf8("toolButton_4"))
        self.textEdit = QtGui.QTextEdit(self.groupBox)
        self.textEdit.setGeometry(QtCore.QRect(10, 50, 391, 341))
        self.textEdit.setObjectName(_fromUtf8("textEdit"))
        self.groupBox_2 = QtGui.QGroupBox(Dialog)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 10, 411, 431))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.toolButton_5 = QtGui.QToolButton(self.groupBox_2)
        self.toolButton_5.setGeometry(QtCore.QRect(300, 20, 101, 21))
        self.toolButton_5.setObjectName(_fromUtf8("toolButton_5"))
        self.textEdit_2 = QtGui.QTextEdit(self.groupBox_2)
        self.textEdit_2.setGeometry(QtCore.QRect(10, 50, 391, 341))
        self.textEdit_2.setObjectName(_fromUtf8("textEdit_2"))
        self.toolButton_6 = QtGui.QToolButton(self.groupBox_2)
        self.toolButton_6.setGeometry(QtCore.QRect(190, 400, 101, 21))
        self.toolButton_6.setObjectName(_fromUtf8("toolButton_6"))
        self.toolButton_2 = QtGui.QToolButton(self.groupBox_2)
        self.toolButton_2.setGeometry(QtCore.QRect(300, 400, 101, 21))
        self.toolButton_2.setObjectName(_fromUtf8("toolButton_2"))

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.groupBox.setTitle(_translate("Dialog", "Ediçao e definição de classes", None))
        self.toolButton_3.setText(_translate("Dialog", "Ligar edição", None))
        self.toolButton.setText(_translate("Dialog", "Salvar edição", None))
        self.toolButton_4.setText(_translate("Dialog", "Cancelar edição", None))
        self.textEdit.setHtml(_translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">//Exibir lista (planilha) das classes de acordo com o critério selecionado. Só permitir alteração quando edição estiver ligada</span></p></body></html>", None))
        self.groupBox_2.setTitle(_translate("Dialog", "Edição e definição de critérios", None))
        self.toolButton_5.setText(_translate("Dialog", "Ligar edição", None))
        self.textEdit_2.setHtml(_translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">//Exibir lista (planilha) de critérios. Só permitir alteração quando edição estiver ligada</span></p></body></html>", None))
        self.toolButton_6.setText(_translate("Dialog", "Cancelar edição", None))
        self.toolButton_2.setText(_translate("Dialog", "Salvar edição", None))

